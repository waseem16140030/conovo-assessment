const navbar = document.getElementById("conovo-nav");

window.addEventListener("scroll", () => {
  if (window.scrollY >= 70) {
    navbar.classList.add("nav-scrolled");
  } else {
    navbar.classList.remove("nav-scrolled");
  }
});

document.addEventListener("DOMContentLoaded", function () {
  function handleNavbarClass() {
    if (window.innerWidth < 768) {
      navbar.classList.remove("fixed-top");
    } else {
      navbar.classList.add("fixed-top");
    }
  }
  handleNavbarClass();
  // Check on window resize
  window.addEventListener("resize", handleNavbarClass);
});
